+++
title = "iSH体验"
description = "iOS上的Linux模拟器"
date = 2022-12-16T09:12:00+08:00
lastmod = 2022-12-16T10:26:00+08:00
tags = ["iOS", "Linux"]
categories = ["软件", "开源"]
draft = true
featuredImage = "ox-hugo/IMG_87600.png"
+++

之前在安卓上介绍过[Termux](https://xlbil.netlify.app/posts/emacs-on-termux/)这个安卓上的Shell模拟器，而在iOS平台上也有一个类似的应用，叫iSH。在App Store上的页面可以看到只有一个白色的截图，不看清楚还以为只是一个低级的ssh应用，其实这个应用内置了一个微型的[Linux系统](https://www.alpinelinux.org/)，而且自带了软件包管理器 `apk` 。


## 体验 {#体验}

首先是操作界面。iSH在键盘上增加了 `TAB` `CTRL` `ESC` 和方向键这四个键，不过这个方向键的操作方式很奇怪，四个方向键是并在一起的，在这个按钮上拖动才能操作方向键。这个设计可能是iSH对于终端不支持点击的补偿（原因见[此处](#外层)）。

iSH默认主题是白底黑字，比较难受，在选项里可以改成黑色主题。里面还有一个选项可以更改iSH的程序图标，有各种用户设计的不同图标。

在iOS自带的文件管理器里开启一个选项还可以查看内置Linux的所有文件。


### 内置系统 {#内置系统}

iSH不像Termux，由于自己内置了一个完整的Linux发行版，所以软件包的选择比较多，而且和Termux不同，Termux里的软件包是单独放在一个特殊的源维护的，iSH不是这样，所以可以换成清华的Alpine Linux源。

```shell
sed -i 's/dl-cdn.alpinelinux.org/mirrors.tuna.tsinghua.edu.cn/g' /etc/apk/repositories
```

Alpine源里的默认Emacs是经过阉割的，里面没有自带文档，需要安装额外的 `emacs-doc` 包。

接下来的内容就是iSH的问题了。


## 问题 {#问题}

iSH的问题目前看下来特别大。首先是卡顿。在应用中切换输入法，进行分屏，或者仅仅是进行一些普通操作，都会导致iSH出现卡顿，经常出现的卡顿就是下面这种情况：

{{< figure src="/ox-hugo/截屏 2022-12-11 22.19.52.png" caption="<span class=\"figure-number\">Figure 1: </span>最容易复现的bug：重复出现的内容" >}}

这还是幸运的情况，如果你很不幸，还会偶尔出现应用黑屏，此时你啥也看不见，只有切换输入法或者其他操作时才能正常。这导致本文在这个应用里的编辑体验非常差。


### <span class="org-todo todo TODO">TODO</span> 性能 {#性能}

这个问题是一个大问题，要从两方面来讲。


#### 底层 {#底层}

Termux和iSH的底层其实不一样：Termux是在Android arm架构的基础上构建的。而iSH则是实现了一个模拟器，在这个基于x86的模拟器上面运行Linux系统。

至于不能基于iOS的原因也是无奈之举，因为苹果的封闭性，iSH如果引用关乎系统的组件会导致无法在App Store过审。即使是基于模拟器iSH也[曾经被App Store下架](https://ish.app/blog/app-store-removal)。


#### 外层 {#外层}

iSH的外层其实是基于一个浏览器——Webview魔改的，使用网页做前端总会出现各种各样的问题，比如切换后台时iSH会刷新


## <span class="org-todo todo TODO">TODO</span> 总结 {#总结}

{{< figure src="/ox-hugo/IMG_87600.png" caption="<span class=\"figure-number\">Figure 2: </span>Git on iSH" >}}
