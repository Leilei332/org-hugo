+++
title = "一个启动器可以有多简洁"
description = "KISS Launcher体验"
date = 2022-12-19T07:32:00+08:00
tags = ["启动器", "Android"]
categories = ["软件", "开源"]
draft = true
+++

> Keep it simple &amp; stupid. -- Kelly Johnson

这句话在很多情况下都适用。在做任何设计时都需要贯彻“简洁”原则。

在程序设计中，和这句话相近的话还有[Worse is better](https://zh.m.wikipedia.org/wiki/%25E6%259B%25B4%25E7%25B3%259F%25E5%25B0%25B1%25E6%2598%25AF%25E6%259B%25B4%25E5%25A5%25BD)，一个程序有时没必要一直增加功能，有时把一件事做好反而是更好的。

最近想在Fdroid找一个比系统内置的华为桌面更好的桌面应用[^fn:1]，然后无意间发现了KISS Launcher这个应用，当时我下载了一个图标包，想着没地方用就下载了这个应用。

到开始用的时候，我还是被这个应用的设计惊艳到了。


## 简洁 {#简洁}

在用这个启动器之前，我还在网上找了一个安卓原生自带的桌面Launcher3（Android10自带版本），默认的桌面非常简洁，实现了基本的主屏幕，应用抽屉功能。整个软件的大小也很小，大概是3~4MB。而KISS Launcher的大小则是以KB为单位计算的！大约800KB左右。

在KISS Launcher中，没有“主屏幕”这个念，显示屏幕是不能放应用图标的。默认显示的是操作的提示信息，在桌面设置中可以设置主屏幕放置小部件，仅此而已。这一点的设计有点像Linux里的[Gnome桌面(3.x版本)](https://gnome.org)[^fn:2]。而最底部则显示了收藏的应用和一个，搜索栏的左边是一个按钮，被称为“KISS圆环”，打开就能看到所有应用的列表，类似于抽屉。

收藏栏能显示的应用不多，所以关键就是这个搜索栏了，而这个搜索栏也确实很强大。[^fn:3]


## <span class="org-todo todo NEXT">NEXT</span> 搜索 <span class="tag"><span class="___">进行中</span></span> {#搜索}

普通的桌面都是要切换到主屏幕才能用搜索功能，而KISS Launcher的搜索功能不但快，而且更方便。在安卓5和更高版本中，长按HOME键可以直接启动搜索，避免要切换到桌面这一操作。

目前KISS Launcher的功能已经很完善了，实现了以下功能：

-   搜索应用
-   搜索联系人，电话号码
-   搜索网络内容
-   计算器


### <span class="org-todo todo TODO">TODO</span> 网络搜索 {#网络搜索}

KISS Launcher默认用谷歌做搜索引擎，里面也内置了很多其他搜索引擎，比如必应、DuckDuckGo等。在设置里面也可以添加一个自定义搜索引擎。比如自定义一个维基百科搜索引擎：


## <span class="org-todo todo TODO">TODO</span> 自定义 {#自定义}

KISS Launcher的自定义功能也很丰富。除了应用界面的主题色可以调整，还有很多细化的选项。


### 手势 {#手势}

这是一个比较实用的功能，目前用的是以下设置：

-   **上划** ：显示应用列表
-   **下划** ：显示通知阴影
-   **左划** ：显示应用列表
-   **长按** ：显示菜单（Display Menu）


### 应用标签 {#应用标签}

这也是一个不错的功能。KISS Launcher会给应用自动打上标签分类。比如“联系人”应用会呼叫，电话这些标签，在搜索里显示。你也可以自己给应用加上标签。

可惜的是，KISS Launcher只识别出了一个联系人和电子邮件应用，所以这个功能还是不太实用。


### 其他 {#其他}

对于一些应用的细节问题，KISS Launcher也提供了一些“强迫症选项。

搜索栏左边的按钮会显示应用的图标，不过在华为手机上这个图标很丑，在用户体验 &rarr; 隐藏KISS圆环就可以隐藏。


## 不足 {#不足}

虽然我一开始是想用图标包而用KISS Launcher的，但是图标包在KISS Launcher上体验并不好。可能是KISS Launcher的代码太少，优化不好，应用了图标包之后会导致KISS Launcher刷新应用的速度非常慢，每次切到主屏幕都要等很久，非常烦人。

这个问题目前无解，开“隐藏图标选项也不能解决。作为对比，Lawnchair这个桌面应用图标包后非常流畅，没有什么问题。


## <span class="org-todo todo TODO">TODO</span> 总结 {#总结}

{{< figure src="/ox-hugo/Screenshot_2022-11-30-20-54-46.png" caption="<span class=\"figure-number\">Figure 1: </span>简洁的KISS Launcher（带Arcticons Icon Pack）" >}}

[^fn:1]: 由于安卓桌面开放的特性，你可以在系统的设置菜单里自己选择支持的应用作为桌面。
[^fn:2]: Gnome桌面和其他Linux桌面不同，遵循着简洁的原则，桌面上不能放置图标，也不显示Dock栏。
[^fn:3]: 这一点也可以参考Gnome桌面的搜索功能。