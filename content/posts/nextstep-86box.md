+++
title = "Nextstep体验"
description = "NeXTSTEP on 86box"
date = 2022-11-06T12:38:00+08:00
tags = ["NeXTSTEP", "86box"]
categories = ["模拟器"]
draft = true
+++

之前在86box里安装Nextstep时发现里面自带的Emacs版本不支持Nextstep的GUI，原来Nextstep

在今天搜索时发现了[这个网站](http://www.nextcomputers.org/)，里面包含了很多关于Nextstep的资源，不止是Emacs，还有很多软件，游戏等。


## <span class="org-todo todo TODO">TODO</span> Emacs {#emacs}

Emacs是在19.x版本才支持GUI的，图标还是一头牛。

{{< figure src="/ox-hugo/86Box_vppuLBgWSV.png" caption="<span class=\"figure-number\">Figure 1: </span>Emacs on" >}}
